from distutils.core import setup, Extension

setup(
    author="Nikakto",
    author_email="mcgish@gmail.com",
    description="Package to work with eve online process memory",

    name = 'evemem', 
    version = '0.01', 
    ext_modules = [
        Extension(
            'evemem', 
            [
                'python/module.cpp', 
                'python/pyeveprocess.cpp',
                'source/eveprocess.cpp',
            ], 
            extra_compile_args=['/std:c++latest'],),
    ],

)
