#ifndef EVEPROCESS_H
#define EVEPROCESS_H

#include <map>
#include <string>
#include <vector>
#include <Windows.h>


class EveOnlineProcess {
    private:
        int pid;
        HANDLE handle;
        SYSTEM_INFO system_info;
        std::map<uint64_t, std::string> cache_types;

    public:
        const int WORD_SIZE = 8;

        EveOnlineProcess(unsigned long pid);
        ~EveOnlineProcess();

        HANDLE getHandle();

        bool readBool(uint64_t address);
        uint64_t readBytes(uint64_t address, SIZE_T length, LPVOID buff);
        float readFloat(uint64_t address);
        int32_t readInt32(uint64_t address);
        int64_t readInt64(uint64_t address);
        std::string readString(uint64_t address, SIZE_T length);
        std::string readType(uint64_t address);
        uint64_t readUInt64(uint64_t address);
};


#endif