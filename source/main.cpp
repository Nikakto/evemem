#include <iostream>
#include <time.h>
#include <windows.h>

#include "eveprocess.h"


using namespace std;


int main() {
	cout << "Run..." << endl;

	EveOnlineProcess process(8972);
	if (!process.getHandle()) {
		throw runtime_error("Cannot open eve online process");
	}

	const clock_t begin_time = clock();
	for (int i = 0; i < 1; i++) {
		cout << process.readType(0X2CC01151AC8) << endl;
	}
	
	cout << float( clock () - begin_time ) /  CLOCKS_PER_SEC << endl;
	Sleep(5000);
}