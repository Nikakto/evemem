#include <iostream>
#include <string>
#include <Windows.h>

#include "eveprocess.h"

using namespace std;


EveOnlineProcess::EveOnlineProcess(unsigned long pid) : pid(pid) {
	GetSystemInfo(&system_info);

	HANDLE eve_handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
    if (eve_handle == NULL) {
		cerr << "Cannot open Eve Online process " << pid << endl;
		handle = 0;
	} else {
		handle = eve_handle;
	}
}


EveOnlineProcess::~EveOnlineProcess() {
	CloseHandle(handle);
}


HANDLE EveOnlineProcess::getHandle() {
	return handle;
}


bool EveOnlineProcess::readBool(uint64_t address) {
	BYTE value;
	readBytes(address, 1, &value);
	return value > 0;
}


uint64_t EveOnlineProcess::readBytes(uint64_t address, SIZE_T length, LPVOID buff) {
	LPCVOID pointer = (LPCVOID)address;

	if (pointer > system_info.lpMaximumApplicationAddress) {
		return 0;
	}

	// Very slow, hope ReadProcessMemory will not return zero on error
	// MEMORY_BASIC_INFORMATION  memory_info;
	// VirtualQueryEx(handle, pointer, &memory_info, sizeof(memory_info));
	// if (!(memory_info.Protect & SECTION_ALL_ACCESS) || memory_info.State != MEM_COMMIT) {
	// 	return 0;
	// }

	SIZE_T lpNumberOfBytesRead = 0;
	if (!ReadProcessMemory(handle, pointer, buff, length, &lpNumberOfBytesRead)) {
		return 0;
	}
	return lpNumberOfBytesRead;
}


float EveOnlineProcess::readFloat(uint64_t address) {
	float value;
	readBytes(address, WORD_SIZE, (BYTE*)&value);
	return value;
}


int32_t EveOnlineProcess::readInt32(uint64_t address) {
	int32_t result;
	readBytes(address, 4, (BYTE*)&result);
	return result;
}


int64_t EveOnlineProcess::readInt64(uint64_t address) {
	int64_t result;
	readBytes(address, 8, (BYTE*)&result);
	return result;
}


string EveOnlineProcess::readString(uint64_t address, SIZE_T length) {
	BYTE* data;
	data = new BYTE[length];
	readBytes(address, length, data);
	string value = (char*)data;
	delete data;
	return value;
}


string EveOnlineProcess::readType(uint64_t address) {
	uint64_t address_of_type = readUInt64(address + WORD_SIZE);
	if (!address_of_type) {
		return "";
	}

	if (cache_types.find(address_of_type) != cache_types.end()) {
		return cache_types[address_of_type];
	}

	uint64_t address_of_type_repr = readUInt64(address_of_type + 3 * WORD_SIZE);
    if (!address_of_type_repr) {
		return "";
	}
    
	BYTE type_repr[32];
	for (int index = 1; index < 32; index++) {
		readBytes(address_of_type_repr, index, type_repr);

		BYTE last_char_code = type_repr[index - 1];
		if (!isascii(last_char_code)) {
			break;
		}

		if (last_char_code == 0) {
			string type = (char*)type_repr;
			cache_types[address_of_type] = type;
			return type;
		}
	}

	return "";
}


uint64_t EveOnlineProcess::readUInt64(uint64_t address) {
	uint64_t result;
	readBytes(address, 8, (BYTE*)&result);
	return result;
}
