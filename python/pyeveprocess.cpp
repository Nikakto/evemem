#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <string>
#include <structmember.h>

#include "pyeveprocess.h"



static void PyEveOnlineProcess_dealloc(PyObjectEveOnlineProcess* self) {
    Py_XDECREF(self->pid);
    delete self->process;
    Py_TYPE(self)->tp_free((PyObject *)self);
}


static PyObject* PyEveOnlineProcess_init(PyObjectEveOnlineProcess *self, PyObject *args) {
    if (!PyArg_ParseTuple(args, "k", &self->pid)) {
        PyErr_SetString(PyExc_ValueError, "EveOnlineProcess init error");
        PyErr_Occurred();
    }

    printf("%lu", self->pid);
    self->process = new EveOnlineProcess((uint32_t)self->pid);
    if (!self->process->getHandle()) {
        PyErr_SetString(PyExc_BaseException, "EveOnlineProcess init error");
        PyErr_Occurred();
    }
    Py_RETURN_NONE;
}


static PyObject* PyEveOnlineProcess_readBytes(PyObjectEveOnlineProcess *self, PyObject *args) {
    uint64_t address, length;
    if (!PyArg_ParseTuple(args, "KK", &address, &length)) {
        PyErr_SetString(PyExc_ValueError, "EveOnlineProcess init error");
        PyErr_Occurred();
    }

    BYTE *buff;
    buff = new BYTE[length];
    uint64_t lpNumberOfBytesRead = self->process->readBytes(address, length, &buff[0]);

    PyObject *result = Py_BuildValue("y#", buff, lpNumberOfBytesRead);
    delete buff;
    return result;
}


static PyObject* PyEveOnlineProcess_readType(PyObjectEveOnlineProcess *self, PyObject *args) {
    uint64_t address;
    if (!PyArg_ParseTuple(args, "K", &address)) {
        PyErr_SetString(PyExc_ValueError, "EveOnlineProcess init error");
        PyErr_Occurred();
    }

    std::string type = self->process->readType(address);
    return Py_BuildValue("s", type.c_str(), type.max_size());
}