#ifndef PY_EVEPROCESS_H
#define PY_EVEPROCESS_H

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "../source/eveprocess.h"

typedef struct {
    PyObject_HEAD
    PyObject *pid   = NULL;

    EveOnlineProcess *process;
} PyObjectEveOnlineProcess;

static void PyEveOnlineProcess_dealloc(PyObjectEveOnlineProcess* self);
static PyObject* PyEveOnlineProcess_init(PyObjectEveOnlineProcess *self, PyObject *args);
static PyObject* PyEveOnlineProcess_readBytes(PyObjectEveOnlineProcess *self, PyObject *args);
static PyObject* PyEveOnlineProcess_readType(PyObjectEveOnlineProcess *self, PyObject *args);

#endif