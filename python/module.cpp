#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "pyeveprocess.cpp"


// class EveOnlineProcess:
static PyMemberDef EveOnlineProcess_members[] = {
    {"pid", T_INT, offsetof(PyObjectEveOnlineProcess, pid), 0, "Eve online process pid"},
    {NULL}  /* Sentinel */
};


static PyMethodDef eveonlineprocess_methods[] = { 
    {
        "read_bytes", (PyCFunction)PyEveOnlineProcess_readBytes, METH_VARARGS,
        "Read bytes from eve online thread, return empty str on error"
    },
    {
        "read_type", (PyCFunction)PyEveOnlineProcess_readType, METH_VARARGS,
        "Read python data type at address"
    },
    {NULL, NULL, 0, NULL}
};


static PyTypeObject PyTypeEveOnlineProcess = {
    .tp_name = "evemem.EveOnlineProcess",
    .tp_basicsize = sizeof(PyObjectEveOnlineProcess),
    .tp_itemsize = 0,

    /* Methods to implement standard operations */
    .tp_dealloc = (destructor)PyEveOnlineProcess_dealloc,

    /* Flags to define presence of optional/expanded features */
    .tp_flags = Py_TPFLAGS_DEFAULT,

    .tp_doc = "Eve online memory reader",

    /* Attribute descriptor and subclassing stuff */
    .tp_methods = eveonlineprocess_methods,
    .tp_members = EveOnlineProcess_members,
    .tp_init = (initproc)PyEveOnlineProcess_init,
    .tp_new = PyType_GenericNew,
};


// Setup module
static PyModuleDef evemem_module = {
    .m_name = "evemem",
    .m_doc = "Package to work with eve online process memory",
    .m_size = -1,
};


// Init module
PyMODINIT_FUNC PyInit_evemem(void) {
    PyObject *module;
    if (PyType_Ready(&PyTypeEveOnlineProcess) < 0)
        return NULL;

    module = PyModule_Create(&evemem_module);
    if (module == NULL)
        return NULL;

    Py_INCREF(&PyTypeEveOnlineProcess);
    if (PyModule_AddObject(module, "EveOnlineProcess", (PyObject *) &PyTypeEveOnlineProcess) < 0) {
        Py_DECREF(&PyTypeEveOnlineProcess);
        Py_DECREF(module);
        return NULL;
    }

    return module;
}